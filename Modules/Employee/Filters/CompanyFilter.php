<?php

namespace Modules\Employee\Filters;

use Modules\Employee\Filters\AbastractsFilter;
use Illuminate\Database\Eloquent\Builder;

class CompanyFilter extends AbastractsFilter
{
    protected $filters = [
        'company' => BaseFilter::class
    ];

    public function mappings()
    {
        return [
            'company' => 'company_id',
        ];
    }

    public function filter(Builder $builder, $value)
    {
        return $builder->where('company_id', $value);
    }
}