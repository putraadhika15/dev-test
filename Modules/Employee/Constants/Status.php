<?php 
namespace Modules\Employee\Constants;

class Status 
{
    const ACTIVE = 1;
    const INACTIVE = 2;
    const BLOCK = 3;

    public static function labels(): array
    {
        return [
            self::ACTIVE => 'Active',
            self::INACTIVE => 'Inactive',
            self::BLOCK => 'Block',
        ];
    }
}